import { createApp } from 'vue'
import './style.css'
import App from './App.vue'

const app = createApp(App)

waitForElm('.acme_dog_breeds_node_api').then(() => {
  app.mount('.acme_dog_breeds_node_api')
})

// https://stackoverflow.com/questions/5525071/how-to-wait-until-an-element-exists
function waitForElm(selector) {
  return new Promise(resolve => {
    if (document.querySelector(selector)) {
      return resolve(document.querySelector(selector));
    }

    const observer = new MutationObserver(() => {
      if (document.querySelector(selector)) {
        resolve(document.querySelector(selector));
        observer.disconnect();
      }
    });

    observer.observe(document.body, {
      childList: true,
      subtree: true
    });
  });
}
