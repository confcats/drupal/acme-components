_This module currently requires https://gitlab.com/confcats/drupal/starter_

This includes a set of example PDB Components both using Dog breed metadata.
https://documenter.getpostman.com/view/4016432/the-dog-api/RW81vZ4Z#intro

# Usage
`drush en acme_components`

## acme_dog_breeds_external_api
This component uses an external api -- build the app and add a block

### Build Vite App
- `cd web/modules/contrib/acme-components`
- `npm install && npm run build`

### Add Block Component Instance
- Place Block: "Dogs - List of Breeds (external)" (Structure > Block Layout)

## acme_dog_breeds_node_api
This component uses

When installing `acme_components` module, it will activate a config split also called `acme_components`. The config split will create the following:
- Vocabulary: "Dog Breed"
- Node: "Dog"

### Set Up Vocab & Node Structures and Migrate
1. `drush config-split:import acme_components` or Synchronize Config (Config > Development > Configuration)
2. `drush mim acme_dog_breeds` or Execute through UI (Structure > Migrate > Manage > Acme)

### Build Vite App
- `cd web/modules/contrib/acme-components`
- `npm install && npm run build`

### Add Block Component Instance
- Place Block: "Dogs - List of Breeds (nodes)" (Structure > Block Layout)
