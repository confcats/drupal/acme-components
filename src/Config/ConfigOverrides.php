<?php

namespace Drupal\acme_components\Config;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Config\ConfigFactoryOverrideInterface;
use Drupal\Core\Config\StorageInterface;

/**
 * Configuration override.
 */
class ConfigOverrides implements ConfigFactoryOverrideInterface
{

  /**
   * {@inheritdoc}
   */
  public function loadOverrides($names)
  {
    $overrides = [];
    if (in_array('config_split.config_split.acme_components', $names)) {
      $overrides['config_split.config_split.acme_components'] = ['status' => TRUE];
    }
    return $overrides;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheSuffix() {
    return 'ConfigExampleOverrider';
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheableMetadata($name) {
    return new CacheableMetadata();
  }

  /**
   * {@inheritdoc}
   */
  public function createConfigObject($name, $collection = StorageInterface::DEFAULT_COLLECTION) {
    return NULL;
  }
}
